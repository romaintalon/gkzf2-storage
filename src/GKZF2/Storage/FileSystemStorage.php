<?php

namespace GKZF2\Storage;

use GKZF2\Storage\Exception\MissingWriteRightsException;
use GKZF2\Storage\Exception\NotReferencedMd5Exception;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;

class FileSystemStorage extends AbstractMd5Storage implements ServiceManagerAwareInterface {

    /** @var ServiceManager */
    protected $serviceManager;
    protected $path;

    public function __construct(ServiceManager $serviceManager, $path) {
        $this->setServiceManager($serviceManager);
        $this->path = $path;
    }

    protected function getFilePath($md5) {
        return $this->path . $md5;
    }

    protected function checkWriteRights() {
        if (!is_writable($this->path)) {
            // try create path
            if (@mkdir($this->path, 755, true) && is_writable($this->path)) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $content
     * @return string md5
     * @throws MissingWriteRightsException
     */
    public function saveContent($content)
    {
        $md5 = md5($content);
        // check rights
        if (!$this->checkWriteRights()) {
            throw new MissingWriteRightsException();
        }

        file_put_contents($this->getFilePath($md5), $content);

        return $md5;
    }

    /**
     * @param $md5
     * @return string content
     * @throws NotReferencedMd5Exception
     */
    public function getContent($md5)
    {
        $filePath = $this->getFilePath($md5);
        if (!file_exists($filePath)) {
            throw new NotReferencedMd5Exception();
        }
        return file_get_contents($filePath);
    }

    /**
     * Set service manager
     *
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * @param $md5
     * @return mixed
     * @throws NotReferencedMd5Exception
     */
    public function deleteContent($md5)
    {
        $this->checkWriteRights();
        $filePath = $this->getFilePath($md5);
        if (!file_exists($filePath)) {
            throw new NotReferencedMd5Exception();
        }
        unlink($filePath);
    }
}
