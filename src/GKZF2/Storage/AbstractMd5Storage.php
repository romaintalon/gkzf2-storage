<?php

namespace GKZF2\Storage;

use GKZF2\Storage\Exception\MissingWriteRightsException;
use GKZF2\Storage\Exception\NotReferencedMd5Exception;

abstract class AbstractMd5Storage {
    /**
     * @param $content
     * @return string md5
     * @throws MissingWriteRightsException
     */
    abstract public function saveContent($content);

    /**
     * @param $md5
     * @return string content
     * @throws NotReferencedMd5Exception
     */
    abstract public function getContent($md5);

    /**
     * @param $md5
     * @return mixed
     * @throws NotReferencedMd5Exception
     * @throws MissingWriteRightsException
     */
    abstract public function deleteContent($md5);
}
